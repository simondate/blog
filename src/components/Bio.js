import React from 'react'

// Import typefaces
import 'typeface-montserrat'
import 'typeface-merriweather'

import profilePic from './profile-pic.jpg'
import {rhythm} from '../utils/typography'

class Bio extends React.Component {
  render() {
    return (<div style={{
        display: 'flex',
        marginBottom: rhythm(2.5)
      }}>
      <img src={profilePic} alt={`Simons Date`} style={{
          marginRight: rhythm(1 / 2),
          marginBottom: 0,
          borderRadius: '50%',
          marginTop: 0,
          width: rhythm(2),
          height: rhythm(2)
        }}/>
      <p>
        My experiences from a year of travelling whilst working remotly. Also on
        <a href="https://twitter.com/simondate">Twitter</a>.
      </p>
    </div>)
  }
}

export default Bio
